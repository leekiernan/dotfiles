export DEFAULT_USER="$(whoami)"
export ZSH=~/.oh-my-zsh
ZSH_THEME="kolo"

plugins=(git)

source $ZSH/oh-my-zsh.sh

if [[ -a ~/.localrc ]]
then
  source ~/.localrc
fi

alias ce='code-exploration'
alias bm="blightmud -w tg"
alias ci="code"
alias iown='sudo chown -R `whoami` $1'
alias sc='screen'
alias tm='tmux'
alias tree='find . -print | sed -e "s;[^/]*/;|____;g;s;____|; |;g"'
alias rb='ruby'
alias tmux='tmux -2'
alias spaces='rename "s/\s+/-/g"'
alias underscores='rename "s/_/-/g"'
alias dashes='rename "s/--/-/g"'
alias chars='rename "s/[\]\[)(+%^&*]//g"'
alias exif='mogrify -strip -taint'
alias resample='mogrify -density 72'
alias crop='mogrify -gravity Center -crop 403x230+0+0'
alias be='bundle exec'
alias clean="printf '\33c\e[3J'"
alias flush="sudo killall -HUP mDNSResponder; sleep 2; echo 'Flushed.'";
alias cls="printf '\033c\e[3J'"
alias towebp='for file in *.(jpg|jpeg|png|gif); do name=$(echo "$file" | cut -f 1 -d '.'); cwebp -q 80 "$file" -o "$name.webp"; done'
alias bej='bundle exec jekyll'
alias bake='bundle exec rake'
alias thumbnail="mogrify -thumbnail '487x487>' -background white -gravity center -extent 487x487 *.jpg"
alias res='for file in *.(jpg|jpeg|png|gif); do identify -format "$file = %w x %h x %x\n" $file; done'
# alias heic='mogrify -format jpg *.HEIC'
alias heic='find . -iname "*.HEIC" -exec mogrify -format jpg {} +'
alias flatten="find . -mindepth 2 -type f -exec mv -t . -i '{}' +"
# alias dmongo='docker run --rm -p 27017:27017 -e MONGO_INITDB_ROOT_USERNAME="root" -e MONGO_INITDB_ROOT_PASSWORD="password" --name mongo mongo'
alias dmongo="docker run --rm -d -p 27017:27017 -v mongodbdata:/data/db mongo"
alias nr="npm run"
alias lconfig=" npx --package=@candide/floating-garden@latest configure-local-env"
alias vim=nvim

dkill() {
  if docker info > /dev/null 2>&1; then
    docker stop $(docker ps -a -q)
    docker rm $(docker ps -a -q)
    docker rmi $(docker images -a -q)
  else
    echo "Docker isn't running"
  fi
}

downcase() {
  if [ "$1" != "" ]
  then
    for f in $@; do
      mv "$f" "$f.tmp"
      mv "$f.tmp" "`echo $f | tr "[:upper:]" "[:lower:]"`"
    done
  else
    for f in *; do
      mv "$f" "$f.tmp"
      mv "$f.tmp" "`echo $f | tr "[:upper:]" "[:lower:]"`"
    done
  fi
}

[[ $commands[kubectl] ]] && source <(kubectl completion zsh)
[ -f ~/.kubectl_aliases ] && source ~/.kubectl_aliases

###-begin-npm-completion-###
#
# npm command completion script
#
# Installation: npm completion >> ~/.bashrc  (or ~/.zshrc)
# Or, maybe: npm completion > /usr/local/etc/bash_completion.d/npm
#

if type complete &>/dev/null; then
  _npm_completion () {
    local words cword
    if type _get_comp_words_by_ref &>/dev/null; then
      _get_comp_words_by_ref -n = -n @ -n : -w words -i cword
    else
      cword="$COMP_CWORD"
      words=("${COMP_WORDS[@]}")
    fi

    local si="$IFS"
    IFS=$'\n' COMPREPLY=($(COMP_CWORD="$cword" \
                           COMP_LINE="$COMP_LINE" \
                           COMP_POINT="$COMP_POINT" \
                           npm completion -- "${words[@]}" \
                           2>/dev/null)) || return $?
    IFS="$si"
    if type __ltrim_colon_completions &>/dev/null; then
      __ltrim_colon_completions "${words[cword]}"
    fi
  }
  complete -o default -F _npm_completion npm
elif type compdef &>/dev/null; then
  _npm_completion() {
    local si=$IFS
    compadd -- $(COMP_CWORD=$((CURRENT-1)) \
                 COMP_LINE=$BUFFER \
                 COMP_POINT=0 \
                 npm completion -- "${words[@]}" \
                 2>/dev/null)
    IFS=$si
  }
  compdef _npm_completion npm
elif type compctl &>/dev/null; then
  _npm_completion () {
    local cword line point words si
    read -Ac words
    read -cn cword
    let cword-=1
    read -l line
    read -ln point
    si="$IFS"
    IFS=$'\n' reply=($(COMP_CWORD="$cword" \
                       COMP_LINE="$line" \
                       COMP_POINT="$point" \
                       npm completion -- "${words[@]}" \
                       2>/dev/null)) || return $?
    IFS="$si"
  }
  compctl -K _npm_completion npm
fi
###-end-npm-completion-###

#THIS MUST BE AT THE END OF THE FILE FOR SDKMAN TO WORK!!!
export SDKMAN_DIR="~/.sdkman"
[[ -s "~/.sdkman/bin/sdkman-init.sh" ]] && source "~/.sdkman/bin/sdkman-init.sh"

# The next line updates PATH for the Google Cloud SDK.
if [ -f '/Users/lee/google-cloud-sdk/path.zsh.inc' ]; then . '/Users/lee/google-cloud-sdk/path.zsh.inc'; fi

# The next line enables shell command completion for gcloud.
if [ -f '/Users/lee/google-cloud-sdk/completion.zsh.inc' ]; then . '/Users/lee/google-cloud-sdk/completion.zsh.inc'; fi
